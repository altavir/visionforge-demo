import space.kscience.dataforge.misc.DFExperimental
import space.kscience.visionforge.solid.box
import space.kscience.visionforge.solid.solid
import space.kscience.visionforge.three.makeThreeJsFile

@OptIn(DFExperimental::class)
fun main() {
    makeThreeJsFile {
        vision {
            solid {
                box(100, 100, 100)
            }
        }
    }
}